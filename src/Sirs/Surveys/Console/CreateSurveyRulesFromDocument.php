<?php

namespace Sirs\Surveys\Console;

use Exception;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Artisan;
use Sirs\Surveys\Documents\SurveyDocument;
use Sirs\Surveys\Console\Traits\PathHelpers;

class CreateSurveyRulesFromDocument extends CreateSurveyRules
{
    use PathHelpers;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'survey:rules
                            {document : File location of survey document}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create/update rules file from survey document';

    /**
    * The survey object
    *
    * @var \Sirs\Surveys\Documents\SurveyDocument
    */
    protected $surveyDoc = null;

    public function init()
    {
        $this->surveyDoc =  SurveyDocument::initFromFile($this->convertToAbsolute($this->argument('document')));
        $this->className = $this->surveyDoc->getRulesClassName();
    }

    protected function getSurveyPages()
    {
        return $this->surveyDoc->getPages();
    }
}
