<?php

namespace Sirs\Surveys\Console;

use Exception;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\File;

class CreateSurveyRules extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:survey-rules
                            {className : Name of the rules class}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new survey rules class';

    /**
     * The console command option for class name
     *
     * @var string
     */
    protected $className = null;

    const WRITING_FILE_ERROR = "Error writing to file";

    public function init()
    {
        $this->className = $this->argument('className');
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->init();

        $dir = config('surveys.rulesPath');

        $filename =  $dir . '/'.$this->className .'.php';

        if (!File::exists($dir)) {
            File::makeDirectory($dir, 0775, true);
        }

        if (File::exists($filename)) {
            $this->info('Cannot create the rules class '.$this->className.'. File '.$filename.' already exists.');
        }

        if (!File::put($filename, $this->getRulesText())) {
            throw new Exception(static::WRITING_FILE_ERROR);
        }

        $testName = 'Surveys/'.$this->className.'Test';
        Artisan::call('make:test', ['name'=>$testName]);
        $this->info('Created ' . $filename);
        $this->info(' and test, tests/'.$testName);

        return 0;
    }

    protected function getSurveyPages()
    {
        return [];
    }

    protected function getRulesText()
    {
        $str = str_replace(
            'DummyClass',
            $this->className,
            file_get_contents(__DIR__.'/stubs/rules.stub')
        );

        $pageStr = '';
        $pageTitles = [];
        foreach ($this->getSurveyPages() as $page) {
            $pageTitles[] = $page->getTitle();
        }
        $pageStr = implode("\n\t\t", $pageTitles);
        $str = str_replace('PAGES', $pageStr, $str);

        return $str;
    }
}
