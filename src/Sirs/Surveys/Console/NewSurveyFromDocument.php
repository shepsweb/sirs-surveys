<?php

namespace Sirs\Surveys\Console;

use Exception;
use Illuminate\Console\Command;

class NewSurveyFromDocument extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'survey:new
                            {document : File location of survey document}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Creates new survey migration and rules document';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $survey = $this->argument('document');

        try {
            $this->call('survey:migration', [
                'document' => $survey
            ]);

            $this->call('survey:rules', [
                'document' => $survey
            ]);
            return 0;
        } catch (Exception $e) {
            $this->error($e->getMessage());
            return 1;
        }
    }
}
