<?php

namespace Sirs\Surveys\Console;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;

class RebuildSurveyMigrations extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'survey:migrations-all';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Rebuild migrations for all surveys in the project.';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $dir = config('surveys.surveysPath');
        foreach (scandir($dir) as $filename) {
            if (in_array($filename, ['.', '..']) || is_dir($dir.'/'.$filename) || pathinfo($dir.'/'.$filename)['extension'] != 'xml') {
                continue;
            }
            $this->info('Create migration for '.$filename);
            Artisan::call('survey:migration', ['document' => config('surveys.surveysPath') . '/' . $filename]);
        }

        return 0;
    }
}
