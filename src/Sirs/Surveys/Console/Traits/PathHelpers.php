<?php

namespace Sirs\Surveys\Console\Traits;

trait PathHelpers
{
    protected function convertToAbsolute(string $path)
    {
        if ($path[0] === '/') {
            return $path;
        }

        return base_path($path);
    }
}
