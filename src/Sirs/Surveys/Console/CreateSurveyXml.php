<?php

namespace Sirs\Surveys\Console;

use Exception;
use Illuminate\Support\Str;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;

class CreateSurveyXml extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:survey
                            {name : name of the survey}
                            {--title= : title of survey}
                            {--survey_version=1 : version}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Creates a stub survey file';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $name = ucfirst(Str::camel($this->argument('name')));
        $filename = Str::snake($name);
        $title = $this->option('title') ?? ucwords(str_replace('_', ' ', $filename));
        $version = $this->option('survey_version');

        $path = config('surveys.surveysPath');

        if (!file_exists($path)) {
            mkdir($path, 0777, true);
        }

        $fullFilePath =  "$path/$filename.xml";

        $contents = $this->generateXmlString([
            'DUMMY_NAME' => $name,
            'DUMMY_TITLE' => $title,
            'DUMMY_VERSION' => $version,
        ]);

        if (!File::put($fullFilePath, $contents)) {
            throw new Exception("Error writing to file");
        }

        $this->info("Created $fullFilePath");

        return 0;
    }

    /**
     * Generate xml string
     *
     * @param array $data
     * @return string
     */
    protected function generateXmlString(array $data): string
    {
        $template = file_get_contents(__DIR__ . '/stubs/survey_xml.stub');

        foreach ($data as $key => $value) {
            $template = str_replace($key, $value, $template);
        }

        return $template;
    }
}
