<?php

namespace Sirs\Surveys\Console;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class RefreshSurveyResponseTables extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'survey:refresh {--only= : Comma separated list of survey slugs to refresh}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Refresh response tables';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if (!$this->confirm('Are you sure you want to refresh all (or given) survey responses?')) {
            return 1;
        }

        $surveyQuery = class_survey()::query();

        if ($this->option('only')) {
            $slugs = explode(',', $this->option('only'));
            $surveyQuery->whereIn('slug', $slugs);
        }

        $surveys = $surveyQuery->get();
        $migrationNames = $surveys->pluck('response_table')
                            ->map(function ($rspTable) {
                                return '0000_00_00_000001_create_survey_'.$rspTable;
                            });

        DB::table("migrations")
                        ->whereIn('migration', $migrationNames)
                        ->delete();

        $surveys->pluck('response_table')
            ->each(function ($tableName) {
                Schema::dropIfExists($tableName);
            });

        $this->call('migrate');

        return 0;
    }
}
