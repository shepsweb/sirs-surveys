<?php

namespace Sirs\Surveys\Console;

use Exception;
use Illuminate\Console\Command;
use Sirs\Surveys\Documents\SurveyDocument;
use Sirs\Surveys\Console\Traits\PathHelpers;

class ValidateSurveyDefinition extends Command
{

    use PathHelpers;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'survey:validate {survey : Path to survey xml}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Validate a survey against the schema';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            SurveyDocument::initFromFile($this->convertToAbsolute($this->argument('survey')));
            $this->info('Survey at '.$this->argument('survey').' is valid');
        } catch (Exception $e) {
            throw $e;
        }
    }
}
