<?php

namespace Sirs\Surveys\Tests\Unit;

use Illuminate\Http\Request;
use Sirs\Surveys\Models\Survey;
use Sirs\Surveys\Tests\TestCase;
use Illuminate\Support\Facades\Hash;
use Sirs\Surveys\Tests\Fixtures\User;
use Sirs\Surveys\SurveyControlService;
use Sirs\Surveys\Exceptions\InvalidInputException;
use Sirs\Surveys\Exceptions\SurveyNavigationException;
use Sirs\Surveys\Tests\Fixtures\TestSurveyRules;

class SurveyControlServiceTest extends TestCase
{
    /** @var string */
    protected $documentName;

    protected function setUp(): void
    {
        parent::setUp();

        config()->set('surveys.rulesPath', app_path('Surveys'));
        config()->set('surveys.surveysPath', __DIR__ . '/../Fixtures/surveys');
        config()->set('surveys.useUuidForResponses', false);

        $this->documentName = 'test_survey_one';

        $this->artisan('survey:migration', [
            'document' => './../../../../tests/Fixtures/surveys/' . $this->documentName . '.xml',
        ])->assertExitCode(0);

        $this->artisan('migrate:fresh');

        $this->loadLaravelMigrations([
            '--database' => 'testbench'
        ]);

        $this->user = User::create([
            'name' => 'Test',
            'email' => 'test@test.com',
            'password' => Hash::make('password')
        ]);
    }

    /** @test */
    public function it_can_resolves_current_page_name()
    {
        $survey = Survey::first();
        $surveyResponse = $survey->getNewResponse($this->user);
        $surveyResponse->save();

        $request = new Request;
        $surveyControlService = new SurveyControlService($request, $surveyResponse);
        $this->assertNull($surveyControlService->resolveCurrentPageName());

        $request = new Request(['page' => 'page_1']);
        $surveyControlService = new SurveyControlService($request, $surveyResponse);
        $this->assertEquals('page_1', $surveyControlService->resolveCurrentPageName());

        $request = new Request();
        $surveyResponse->last_page = 'page_1';
        $surveyControlService = new SurveyControlService($request, $surveyResponse);
        $this->assertEquals('page_1', $surveyControlService->resolveCurrentPageName());
    }

    /** @test */
    public function it_does_not_store_when_navigating_previous()
    {
        $survey = Survey::first();
        $surveyResponse = $survey->getNewResponse($this->user);
        $surveyResponse->started_at = null;
        $surveyResponse->save();

        $request = new Request(['nav' => 'prev']);
        $surveyControlService = new SurveyControlService($request, $surveyResponse);
        $this->assertNull($surveyControlService->storeResponseData());

        $this->assertNull($surveyResponse->fresh()->started_at);
    }

    /** @test */
    public function it_throws_an_exception_when_nav_is_not_provided()
    {
        $this->expectException(SurveyNavigationException::class);

        $survey = Survey::first();
        $surveyResponse = $survey->getNewResponse($this->user);
        $surveyResponse->save();

        $request = new Request;
        $surveyControlService = new SurveyControlService($request, $surveyResponse);

        $surveyControlService->followNav();
    }

    /** @test */
    public function it_can_navigate_to_the_next_page()
    {
        $survey = Survey::first();
        $surveyResponse = $survey->getNewResponse($this->user);
        $surveyResponse->save();

        $request = new Request(['nav' => 'next', 'page' => 'page_1']);
        $surveyControlService = new SurveyControlService($request, $surveyResponse);

        $response = $surveyControlService->navigate();

        $this->assertEquals(route('survey_get', [
            'responseId' => $surveyResponse->id,
            'respondentType' => 'sirs-surveys-tests-fixtures-user',
            'respondentId' => $this->user->id,
            'surveySlug' => $survey->slug,
            'page' => 'included_page',
        ]), $response->getTargetUrl());
    }

    /** @test */
    public function it_can_navigate_to_the_previous_page()
    {
        $survey = Survey::first();
        $surveyResponse = $survey->getNewResponse($this->user);
        $surveyResponse->save();

        $request = new Request(['nav' => 'prev', 'page' => 'included_page']);
        $surveyControlService = new SurveyControlService($request, $surveyResponse);

        $response = $surveyControlService->navigate();

        $this->assertEquals(route('survey_get', [
            'responseId' => $surveyResponse->id,
            'respondentType' => 'sirs-surveys-tests-fixtures-user',
            'respondentId' => $this->user->id,
            'surveySlug' => $survey->slug,
            'page' => 'page_1',
        ]), $response->getTargetUrl());
    }

    /** @test */
    public function it_does_not_skip_when_skip_rule_returns_zero()
    {
        $survey = Survey::first();

        $surveyResponse = $survey->getNewResponse($this->user);
        $surveyResponse->save();

        TestSurveyRules::setSkip(0);

        $survey->getSurveyDocument()->setRulesClass(TestSurveyRules::class);
        $surveyResponse->setRelation('survey', $survey);

        $request = new Request(['nav' => 'next', 'page' => 'page_1']);
        $surveyControlService = new SurveyControlService($request, $surveyResponse);

        $response = $surveyControlService->navigate();
        $this->assertEquals(route('survey_get', [
            'responseId' => $surveyResponse->id,
            'respondentType' => 'sirs-surveys-tests-fixtures-user',
            'respondentId' => $this->user->id,
            'surveySlug' => $survey->slug,
            'page' => 'included_page',
        ]), $response->getTargetUrl());
    }

    /** @test */
    public function it_does_skip_when_skip_rule_returns_one()
    {
        $survey = Survey::first();

        $surveyResponse = $survey->getNewResponse($this->user);
        $surveyResponse->save();

        TestSurveyRules::setSkip(1);

        $survey->getSurveyDocument()->setRulesClass(TestSurveyRules::class);
        $surveyResponse->setRelation('survey', $survey);

        $request = new Request(['nav' => 'next', 'page' => 'page_1']);
        $surveyControlService = new SurveyControlService($request, $surveyResponse);

        $response = $surveyControlService->navigate();
        $this->assertEquals(route('survey_get', [
            'responseId' => $surveyResponse->id,
            'respondentType' => 'sirs-surveys-tests-fixtures-user',
            'respondentId' => $this->user->id,
            'surveySlug' => $survey->slug,
            'page' => 'page_3',
        ]), $response->getTargetUrl());
    }

    /** @test */
    public function it_can_finalize_when_skip_rule_is_two()
    {
        $survey = Survey::first();

        $surveyResponse = $survey->getNewResponse($this->user);
        $surveyResponse->finalized_at = null;
        $surveyResponse->save();

        $redirectUrl = route('survey_get', [
            'responseId' => $surveyResponse->id,
            'respondentType' => 'sirs-surveys-tests-fixtures-user',
            'respondentId' => $this->user->id,
            'surveySlug' => $survey->slug,
            'page' => 'page_3',
        ]);

        TestSurveyRules::setSkip(2);
        TestSurveyRules::setRedirectUrl($redirectUrl);

        $survey->getSurveyDocument()->setRulesClass(TestSurveyRules::class);
        $surveyResponse->setRelation('survey', $survey);

        $request = new Request(['nav' => 'next', 'page' => 'page_1']);
        $surveyControlService = new SurveyControlService($request, $surveyResponse);

        $response = $surveyControlService->navigate();
        $this->assertNotNull($surveyResponse->fresh()->finalized_at);
        $this->assertEquals($redirectUrl, $response->getTargetUrl());
    }

    /** @test */
    public function it_can_redirect_if_skip_rule_is_three()
    {
        $survey = Survey::first();

        $surveyResponse = $survey->getNewResponse($this->user);
        $surveyResponse->save();

        $redirectUrl = route('survey_get', [
            'responseId' => $surveyResponse->id,
            'respondentType' => 'sirs-surveys-tests-fixtures-user',
            'respondentId' => $this->user->id,
            'surveySlug' => $survey->slug,
            'page' => 'page_3',
        ]);

        TestSurveyRules::setSkip(3);
        TestSurveyRules::setRedirectUrl($redirectUrl);

        $survey->getSurveyDocument()->setRulesClass(TestSurveyRules::class);
        $surveyResponse->setRelation('survey', $survey);

        $request = new Request(['nav' => 'next', 'page' => 'page_1']);
        $surveyControlService = new SurveyControlService($request, $surveyResponse);

        $response = $surveyControlService->navigate();
        $this->assertEquals($redirectUrl, $response->getTargetUrl());
    }

    /** @test */
    public function it_throws_an_exception_on_invalid_rule()
    {
        $this->expectException(InvalidInputException::class);
        $survey = Survey::first();

        $surveyResponse = $survey->getNewResponse($this->user);
        $surveyResponse->save();

        $redirectUrl = route('survey_get', [
            'responseId' => $surveyResponse->id,
            'respondentType' => 'sirs-surveys-tests-fixtures-user',
            'respondentId' => $this->user->id,
            'surveySlug' => $survey->slug,
            'page' => 'page_3',
        ]);

        TestSurveyRules::setSkip(42);
        TestSurveyRules::setRedirectUrl($redirectUrl);

        $survey->getSurveyDocument()->setRulesClass(TestSurveyRules::class);
        $surveyResponse->setRelation('survey', $survey);

        $request = new Request(['nav' => 'next', 'page' => 'page_1']);
        $surveyControlService = new SurveyControlService($request, $surveyResponse);

        $surveyControlService->navigate();
    }

    /** @test */
    public function it_has_no_redirect()
    {
        $survey = Survey::first();

        $surveyResponse = $survey->getNewResponse($this->user);
        $surveyResponse->save();

        $redirectUrl = 'http://example.com';

        session()->put('survey_previous', $redirectUrl);

        TestSurveyRules::setSkip(3);
        TestSurveyRules::setRedirectUrl(null);

        $survey->getSurveyDocument()->setRulesClass(TestSurveyRules::class);
        $surveyResponse->setRelation('survey', $survey);

        $request = new Request(['nav' => 'next', 'page' => 'page_1']);
        $surveyControlService = new SurveyControlService($request, $surveyResponse);

        $response = $surveyControlService->navigate();

        $this->assertEquals(url($redirectUrl), $response->getTargetUrl());
    }

}
