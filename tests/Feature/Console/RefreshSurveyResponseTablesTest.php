<?php

namespace Sirs\Surveys\Tests\Feature\Console;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\File;
use Sirs\Surveys\Models\Response;
use Sirs\Surveys\Models\Survey;
use Sirs\Surveys\Tests\TestCase;

class RefreshSurveyResponseTablesTest extends TestCase
{
    #use RefreshDatabase;

    /** @var string */
    protected $migrationPath;

    /** @var string */
    protected $documentName;

    protected function setUp(): void
    {
        parent::setUp();

        config()->set('surveys.surveysPath', __DIR__ . '/../../Fixtures/surveys');

        $this->documentName = 'test_survey_one';

        $this->migrationPath = database_path("migrations/0000_00_00_000001_create_survey_rsp_{$this->documentName}_1.php");
    }

    protected function tearDown(): void
    {
        File::delete($this->migrationPath);

        parent::tearDown();
    }

    /** @test */
    public function it_refreshes_survey_responses_if_user_confirms()
    {
        config()->set('surveys.useUuidForResponses', false);

        $this->artisan('survey:migration', [
            'document' =>  './../../../../tests/Fixtures/surveys/' . $this->documentName . '.xml',
        ])->assertExitCode(0);

        $this->artisan('migrate');
        $survey = Survey::first();

        $surveyResponseTable = Response::lookupTable($survey->response_table);
        $response = $surveyResponseTable->create([
            'id' => 1,
            'survey_id' => $survey->id,
            'respondent_type' => 'App\User',
            'respondent_id' => 1,
            'q1' => 'My name is john',
        ]);

        $this->assertDatabaseHas($response->getTable(), [
            'id' => $response->id,
        ]);

        $this->artisan('survey:refresh', [
            '--only' => $survey->slug,
        ])
            ->expectsQuestion('Are you sure you want to refresh all (or given) survey responses?', true)
            ->assertExitCode(0);

        $this->assertDatabaseMissing($response->getTable(), [
            'id' => $response->id,
        ]);
    }

    /** @test */
    public function it_does_not_refresh_survey_responses_if_user_does_not_confirm()
    {
        config()->set('surveys.useUuidForResponses', false);

        $this->artisan('survey:migration', [
            'document' =>  './../../../../tests/Fixtures/surveys/' . $this->documentName . '.xml',
        ])->assertExitCode(0);

        $this->artisan('migrate');
        $survey = Survey::first();

        $surveyResponseTable = Response::lookupTable($survey->response_table);
        $response = $surveyResponseTable->create([
            'id' => 1,
            'survey_id' => $survey->id,
            'respondent_type' => 'App\User',
            'respondent_id' => 1,
            'q1' => 'My name is john',
        ]);

        $this->assertDatabaseHas($response->getTable(), [
            'id' => $response->id,
        ]);

        $this->artisan('survey:refresh', [
            '--only' => $survey->slug,
        ])
            ->expectsQuestion('Are you sure you want to refresh all (or given) survey responses?', false)
            ->assertExitCode(1);

        $this->assertDatabaseHas($response->getTable(), [
            'id' => $response->id,
        ]);
    }
}
