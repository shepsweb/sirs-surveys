<?php

namespace Sirs\Surveys\Tests\Feature\Console;

use Exception;
use Sirs\Surveys\Tests\TestCase;
use Illuminate\Support\Facades\File;
use Sirs\Surveys\Console\CreateSurveyMigrationsFromDocument;

class CreateSurveyMigrationsFromDocumentTest extends TestCase
{
    /** @var string */
    protected $migrationpath;

    /** @var string */
    protected $documentName;

    protected function setUp(): void
    {
        parent::setUp();

        config()->set('surveys.surveysPath', __DIR__ . '/../../Fixtures/surveys');

        $this->documentName = 'test_survey_one';

        $this->migrationPath = database_path("migrations/0000_00_00_000001_create_survey_rsp_{$this->documentName}_1.php");
    }

    protected function tearDown(): void
    {
        File::delete($this->migrationPath);

        parent::tearDown();
    }

    /** @test */
    public function it_makes_a_migration_for_a_document()
    {
        $this->artisan('survey:migration', [
            'document' =>  './../../../../tests/Fixtures/surveys/' . $this->documentName . '.xml',
        ])->assertExitCode(0);

        $this->assertFileExists($this->migrationPath);
    }

    /** @test */
    public function it_makes_a_migration_for_a_document_without_a_file_extension()
    {
        $this->artisan('survey:migration', [
            'document' =>  './../../../../tests/Fixtures/surveys/' . $this->documentName,
        ])->assertExitCode(0);

        $this->assertFileExists($this->migrationPath);
    }

    /** @test */
    public function it_makes_a_migration_for_a_document_not_using_uuid_in_config()
    {
        config()->set('surveys.useUuidForResponses', false);

        $this->artisan('survey:migration', [
            'document' =>  './../../../../tests/Fixtures/surveys/' . $this->documentName . '.xml',
        ])->assertExitCode(0);

        $expectedColumn = '$table->increments(\'id\');';

        $fileContents = file_get_contents($this->migrationPath);

        $this->assertStringContainsString($expectedColumn, $fileContents);
    }

    /** @test */
    public function it_makes_a_migration_for_a_document_using_uuid_in_config()
    {
        config()->set('surveys.useUuidForResponses', true);

        $this->artisan('survey:migration', [
            'document' =>  './../../../../tests/Fixtures/surveys/' . $this->documentName . '.xml',
        ])->assertExitCode(0);

        $expectedColumn = '$table->uuid(\'id\')->primary();';

        $fileContents = file_get_contents($this->migrationPath);

        $this->assertStringContainsString($expectedColumn, $fileContents);
    }

    /** @test */
    public function it_throws_an_exception_when_it_cannot_write_to_file()
    {
        File::shouldReceive('put')->andReturn(false);
        File::partialMock();

        $this->expectException(Exception::class);

        $this->artisan('survey:migration', [
            'document' =>  './../../../../tests/Fixtures/surveys/' . $this->documentName . '.xml',
        ]);
    }

    /** @test */
    public function it_makes_a_migration_for_a_document_for_survey_with_id()
    {
        $this->documentName = "test_survey_with_id";
        $this->migrationPath = database_path("migrations/0000_00_00_000001_create_survey_rsp_{$this->documentName}_1.php");

        $this->artisan('survey:migration', [
            'document' =>  './../../../../tests/Fixtures/surveys/' . $this->documentName . '.xml',
        ])->assertExitCode(0);

        $expectedSurveyId = '"id"=>99';

        $fileContents = file_get_contents($this->migrationPath);

        $this->assertStringContainsString($expectedSurveyId, $fileContents);
    }

    /** @test */
    public function it_throws_an_exception_when_the_document_file_has_an_invalid_type()
    {
        $this->expectException(Exception::class);

        $migrationDocument = new CreateSurveyMigrationsFromDocument;
        $migrationDocument->getMigrationDataType('not valid type');
    }

    /** @test */
    public function it_adds_size_of_type_when_given()
    {
        $migrationDocument = new CreateSurveyMigrationsFromDocument;
        $type = $migrationDocument->getMigrationDataType('varchar(20)');

        $this->assertEquals((object) [
            'name' => 'string',
            'size' => '20',
        ], $type);
    }

    /**
     * @test
     * @dataProvider getMigrationDataTypeFormats
     */
    public function it_can_get_migration_type_from_provided_format($sqlFormat, $localFormat)
    {
        $migrationDocument = new CreateSurveyMigrationsFromDocument;
        $type = $migrationDocument->getMigrationDataType($sqlFormat);

        $this->assertEquals((object) [
            'name' => $localFormat,
            'size' => null,
        ], $type);
    }

    public function getMigrationDataTypeFormats()
    {
        return [
            ['tinyint', 'tinyInteger'],
            ['smallint', 'smallInteger'],
            ['mediumint', 'mediumInteger'],
            ['int', 'integer'],
            ['bigint', 'bigInteger'],
            ['float', 'float'],
            ['double', 'double'],
            ['decimal', 'decimal'],
            ['bit', 'bit'],
            ['char', 'char'],
            ['varchar', 'string'],
            ['tinytext', 'string'],
            ['text', 'text'],
            ['mediumtext', 'mediumText'],
            ['longtext', 'longText'],
            ['json', 'json'],
            ['binary', 'binary'],
            ['varbinary', 'binary'],
            ['tinyblob', 'text'],
            ['blob', 'text'],
            ['longblob', 'longText'],
            ['enum', 'enum'],
            ['set', 'enum'],
            ['date', 'date'],
            ['datetime', 'datetime'],
            ['time', 'time'],
            ['timestamp', 'timestamp'],
            ['year', 'date'],
        ];
    }
}
