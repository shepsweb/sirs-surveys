<?php

namespace Sirs\Surveys\Tests\Feature\Console;

use Illuminate\Support\Facades\File;
use Sirs\Surveys\Tests\TestCase;

class RebuildSurveyMigrationsTest extends TestCase
{
    protected function setUp(): void
    {
        parent::setUp();

        $rulesDir = app_path('Surveys');

        config()->set('surveys.surveysPath', __DIR__ . '/../../Fixtures/rebuild_surveys');

        $this->migrationPaths = [
            database_path("migrations/0000_00_00_000001_create_survey_rsp_test_survey_one_1.php"),
            database_path("migrations/0000_00_00_000001_create_survey_rsp_test_survey_two_1.php"),
            database_path("migrations/0000_00_00_000001_create_survey_rsp_test_survey_invalid_1.php")
        ];
    }

    protected function tearDown(): void
    {
        File::delete($this->migrationPaths[0]);
        File::delete($this->migrationPaths[1]);

        parent::tearDown();
    }

    /** @test */
    public function it_can_rebuild_survey_migrations()
    {
        $this->artisan('survey:migrations-all')->assertExitCode(0);

        $this->assertFileExists($this->migrationPaths[0]);
        $this->assertFileExists($this->migrationPaths[1]);

        // test_survey_invalid.txt
        $this->assertFileDoesNotExist($this->migrationPaths[2]);
    }
}
