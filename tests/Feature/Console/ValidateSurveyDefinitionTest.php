<?php

namespace Sirs\Surveys\Tests\Feature\Console;

use Exception;
use Sirs\Surveys\Tests\TestCase;

class ValidateSurveyDefinitionTest extends TestCase
{
    protected function setUp(): void
    {
        parent::setUp();

        config()->set('surveys.surveysPath', __DIR__ . '/../../Fixtures/surveys');
    }

    /** @test */
    public function it_validates_a_survey_xml_file()
    {
        $this->artisan('survey:validate', [
            'survey' =>  './../../../../tests/Fixtures/surveys/test_survey_one.xml',
        ])->assertExitCode(0);
    }

    /** @test */
    public function it_validates_an_invalid_survey_xml_file()
    {
        $this->expectException(Exception::class);

        $this->artisan('survey:validate', [
            'survey' =>  './../../../../tests/Fixtures/surveys/test_survey_invalid.xml',
        ]);
    }
}
