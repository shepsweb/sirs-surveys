<?php

namespace Sirs\Surveys\Tests\Feature\Console;

use Exception;
use Sirs\Surveys\Tests\TestCase;
use Illuminate\Support\Facades\File;

class NewSurveyFromDocumentTest extends TestCase
{
    protected function setUp(): void
    {
        parent::setUp();

        $rulesDir = app_path('Surveys');

        config()->set('surveys.surveysPath', __DIR__ . '/../../Fixtures/surveys');
        config()->set('surveys.rulesPath', $rulesDir);

        $this->documentName = 'test_survey_one';

        $this->migrationPath = database_path("migrations/0000_00_00_000001_create_survey_rsp_{$this->documentName}_1.php");
        $this->rulesPath = $rulesDir . '/Test_survey_one1Rules.php';
    }

    protected function tearDown(): void
    {
        File::delete($this->migrationPath);
        File::delete($this->rulesPath);

        parent::tearDown();
    }

    /** @test */
    public function it_creates_a_new_survey()
    {
        $this->artisan('survey:new', [
            'document' => './../../../../tests/Fixtures/surveys/' .  $this->documentName . '.xml',
        ])->assertExitCode(0);

        $this->assertFileExists($this->migrationPath);
        $this->assertFileExists($this->rulesPath);
    }

    /** @test */
    public function it_throws_an_exception_when_given_an_invalid_survey()
    {
        $this->artisan('survey:new', [
            'document' => './../../../../tests/Fixtures/surveys/test_survey_invalid.xml',
        ])->assertExitCode(1);
    }
}
