<?php

namespace Sirs\Surveys\Tests\Feature\Console;

use Exception;
use Illuminate\Support\Facades\File;
use Sirs\Surveys\Console\CreateSurveyRulesFromDocument;
use Sirs\Surveys\Tests\TestCase;

class CreateSurveyRulesFromDocumentTest extends TestCase
{
    /** @var string */
    protected $documentName;

    /** @var string */
    protected $rulesPath;

    protected function setUp(): void
    {
        parent::setUp();

        $rulesDir = app_path('Surveys');

        config()->set('surveys.surveysPath', __DIR__ . '/../../Fixtures/surveys');

        config()->set('surveys.rulesPath', $rulesDir);

        $this->documentName = 'test_survey_one';

        $this->rulesPath = $rulesDir . '/Test_survey_one1Rules.php';
    }

    protected function tearDown(): void
    {
        File::delete($this->rulesPath);

        parent::tearDown();
    }

    /** @test */
    public function it_makes_a_rules_file_for_a_document()
    {
        $this->artisan('survey:rules', [
            'document' => './../../../../tests/Fixtures/surveys/' . $this->documentName . '.xml',
        ])->assertExitCode(0);

        $this->assertFileExists($this->rulesPath);
    }

    /** @test */
    public function it_makes_a_survey_directory_if_it_does_not_exist()
    {
        $surveyPath = app_path('Surveys');
        File::deleteDirectory($surveyPath);

        $this->assertFileDoesNotExist($surveyPath);

        $this->artisan('survey:rules', [
            'document' => './../../../../tests/Fixtures/surveys/' . $this->documentName . '.xml',
        ])->assertExitCode(0);

        $this->assertFileExists($surveyPath);
    }

    /** @test */
    public function it_throws_an_exception_when_it_cannot_write_to_file()
    {
        File::shouldReceive('put')->andReturn(false);
        File::partialMock();

        $this->expectException(Exception::class);
        $this->expectExceptionMessage(CreateSurveyRulesFromDocument::WRITING_FILE_ERROR);

        $this->artisan('survey:rules', [
            'document' => './../../../../tests/Fixtures/surveys/' . $this->documentName . '.xml',
        ]);
    }

    /** @test */
    public function it_makes_a_rules_file_for_a_class()
    {
        $className = 'TestSurveyRules';

        $this->artisan('make:survey-rules', [
            'className' => $className,
        ])->assertExitCode(0);

        $rulesDir = app_path('Surveys');
        $rulesPath = $rulesDir . '/' . $className . '.php';
        $this->assertFileExists($rulesPath);
    }

    /** @test */
    public function it_displays_a_message_if_rules_file_exists()
    {
        $className = 'TestSurveyRules';
        $fileName = config('surveys.rulesPath') . '/' . $className . '.php';

        $this->artisan('make:survey-rules', [
            'className' => $className,
        ])->assertExitCode(0);

        $this->artisan('make:survey-rules', [
            'className' => $className,
        ])
            ->expectsOutput('Cannot create the rules class ' . $className . '. File ' . $fileName . ' already exists.')
            ->assertExitCode(0);
    }
}
