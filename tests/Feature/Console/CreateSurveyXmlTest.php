<?php

namespace Sirs\Surveys\Tests\Feature\Console;

use Exception;
use Illuminate\Support\Facades\File;
use Sirs\Surveys\Tests\TestCase;

class CreateSurveyXmlTest extends TestCase
{
    protected function setUp(): void
    {
        parent::setUp();

        config()->set('surveys.surveysPath', $path = resource_path('surveys'));

        File::deleteDirectory($path);
    }

    /** @test */
    public function it_creates_a_survey_xml_file()
    {
        $this->artisan('make:survey', [
            'name' => 'national_preparedness_survey blah bee bop 90 &',
        ])->assertExitCode(0);

        $expectedName = "/national_preparedness_survey_blah_bee_bop90&.xml";
        $this->assertFileExists(config('surveys.surveysPath') . $expectedName);
    }

    /** @test */
    public function it_creates_a_survey_xml_file_with_a_given_name()
    {
        $this->artisan('make:survey', [
            'name' => 'national_preparedness_survey',
        ])->assertExitCode(0);

        $expectedName = 'NationalPreparednessSurvey';

        $fileContents = file_get_contents(config('surveys.surveysPath') . '/national_preparedness_survey.xml');

        $this->assertStringContainsString('name="' . $expectedName . '"', $fileContents);
    }

    /** @test */
    public function it_creates_a_survey_xml_file_with_a_given_title()
    {
        $this->artisan('make:survey', [
            'name' => 'national_preparedness_survey',
            '--title' => $title = 'National Preparedness Survey',
        ])->assertExitCode(0);

        $fileContents = file_get_contents(config('surveys.surveysPath') . '/national_preparedness_survey.xml');

        $this->assertStringContainsString('title="' . $title . '"', $fileContents);
    }

    /** @test */
    public function it_creates_a_survey_xml_file_and_auto_generates_title_from_name()
    {
        $this->artisan('make:survey', [
            'name' => 'national_preparedness_survey',
        ])->assertExitCode(0);

        $expectedTitle = 'National Preparedness Survey';

        $fileContents = file_get_contents(config('surveys.surveysPath') . '/national_preparedness_survey.xml');

        $this->assertStringContainsString('title="' . $expectedTitle . '"', $fileContents);
    }

    /** @test */
    public function it_creates_a_survey_xml_file_with_a_given_version()
    {
        $this->artisan('make:survey', [
            'name' => 'national_preparedness_survey',
            '--survey_version' => 2,
        ])->assertExitCode(0);

        $fileContents = file_get_contents(config('surveys.surveysPath') . '/national_preparedness_survey.xml');

        $this->assertStringContainsString('version="2"', $fileContents);
    }

    /** @test */
    public function it_creates_a_survey_xml_file_with_the_default_version_of_1()
    {
        $this->artisan('make:survey', [
            'name' => 'national_preparedness_survey',
        ])->assertExitCode(0);

        $fileContents = file_get_contents(config('surveys.surveysPath') . '/national_preparedness_survey.xml');

        $this->assertStringContainsString('version="1"', $fileContents);
    }

    /** @test */
    public function it_create_a_directory_if_it_does_not_exist()
    {
        $path = config('surveys.surveysPath');

        $this->assertDirectoryDoesNotExist($path);

        $this->artisan('make:survey', [
            'name' => 'national_preparedness_survey',
        ])->assertExitCode(0);

        $this->assertDirectoryExists($path);
    }

    /** @test */
    public function it_throws_an_exception_when_it_cannot_write_to_file()
    {
        File::shouldReceive('put')->andReturn(false);
        File::partialMock();

        $this->expectException(Exception::class);

        $this->artisan('make:survey', [
            'name' => 'national_preparedness_survey',
        ]);
    }
}
