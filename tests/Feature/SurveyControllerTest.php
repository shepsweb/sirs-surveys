<?php

namespace Sirs\Surveys\Tests\Feature\Console;

use Illuminate\Support\Carbon;
use Sirs\Surveys\Models\Survey;
use Sirs\Surveys\Tests\TestCase;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Hash;
use Illuminate\Contracts\Http\Kernel;
use Sirs\Surveys\Tests\Fixtures\User;
use Illuminate\Session\Middleware\StartSession;

class SurveyControllerTest extends TestCase
{
    /** @var string */
    protected $documentName;

    protected function setUp(): void
    {
        parent::setUp();

        $kernel = app(Kernel::class);
        $kernel->pushMiddleware(StartSession::class);

        config()->set('surveys.rulesPath', app_path('Surveys'));
        config()->set('surveys.surveysPath', __DIR__ . '/../Fixtures/surveys');
        config()->set('surveys.useUuidForResponses', false);

        $this->documentName = 'test_survey_one';

        $this->artisan('migrate:fresh');

        $this->loadLaravelMigrations([
            '--database' => 'testbench'
        ]);

        $this->user = User::create([
            'name' => 'Test',
            'email' => 'test@test.com',
            'password' => Hash::make('password')
        ]);
    }

    /** @test */
    public function it_can_create_a_new_survey_response()
    {
        $this->artisan('survey:migration', [
            'document' => './../../../../tests/Fixtures/surveys/' . $this->documentName . '.xml',
        ])->assertExitCode(0);

        $this->artisan('migrate');

        $survey = Survey::first();

        $this->assertDatabaseMissing($survey->response_table, [
            'survey_id' => $survey->id
        ]);

        $this->get(route('survey_get', [
            'responseId' => 'new',
            'respondentType' => 'sirs-surveys-tests-fixtures-user',
            'respondentId' => $this->user->id,
            'surveySlug' => $survey->slug,
        ]))->assertRedirect();

        $this->assertDatabaseHas($survey->response_table, [
            'survey_id' => $survey->id,
            'respondent_id' => $this->user->id,
            'respondent_type' => User::class
        ]);

    }

    /** @test */
    public function it_can_show_an_existing_survey_response()
    {
        $this->artisan('survey:new', [
            'document' => './../../../../tests/Fixtures/surveys/' . $this->documentName . '.xml',
        ])->assertExitCode(0);

        $this->artisan('migrate');

        $survey = Survey::first();
        $surveyResponse = $survey->getNewResponse($this->user);
        $surveyResponse->save();

        $this->get(route('survey_get', [
            'responseId' => $surveyResponse->id,
            'respondentType' => 'sirs-surveys-tests-fixtures-user',
            'respondentId' => $this->user->id,
            'surveySlug' => $survey->slug,
        ]))
        ->assertOk()
        ->assertSee($survey->document->getTitle());

        $this->assertEquals(1, $survey->responses()->count());
    }

    /** @test */
    public function it_can_show_a_recent_existing_survey_response_without_providing_id()
    {
        $this->artisan('survey:new', [
            'document' => './../../../../tests/Fixtures/surveys/' . $this->documentName . '.xml',
        ])->assertExitCode(0);

        $this->artisan('migrate');

        $survey = Survey::first();
        $surveyResponse1 = $survey->getNewResponse($this->user);
        $surveyResponse1->updated_at = now()->subHour();
        $surveyResponse1->save();

        $surveyResponse2 = $survey->getNewResponse($this->user);
        $surveyResponse2->q1 = $q1answer = 'Survey Response 2 Q1 Answer';
        $surveyResponse2->save();

        $response = $this->get(route('survey_get', [
            'respondentType' => 'sirs-surveys-tests-fixtures-user',
            'respondentId' => $this->user->id,
            'surveySlug' => $survey->slug,
        ]))
        ->assertOk()
        ->assertSee($survey->document->getTitle())
        ->assertSee($q1answer);

        $this->assertEquals(2, $survey->responses()->count());
    }

    /** @test */
    public function it_can_create_a_survey_response_without_providing_id_without_existing_responses()
    {
        $this->artisan('survey:new', [
            'document' => './../../../../tests/Fixtures/surveys/' . $this->documentName . '.xml',
        ])->assertExitCode(0);

        $this->artisan('migrate');

        $survey = Survey::first();

        $this->assertEquals(0, $survey->responses()->count());

        $response = $this->get(route('survey_get', [
            'respondentType' => 'sirs-surveys-tests-fixtures-user',
            'respondentId' => $this->user->id,
            'surveySlug' => $survey->slug,
        ]))
        ->assertOk()
        ->assertSee($survey->document->getTitle());

        $this->assertEquals(1, $survey->responses()->count());
    }

    /** @test */
    public function it_can_save_a_survey_response()
    {
        $this->artisan('survey:migration', [
            'document' => './../../../../tests/Fixtures/surveys/' . $this->documentName . '.xml',
        ])->assertExitCode(0);

        $this->artisan('migrate');

        $survey = Survey::first();
        $surveyResponse = $survey->getNewResponse($this->user);
        $surveyResponse->save();

        $payload = [
            'q1' => 'Survey Response 2 Q1 Answer',
        ];

        $response = $this->post(route('survey_post', [
            'respondentType' => 'sirs-surveys-tests-fixtures-user',
            'respondentId' => $this->user->id,
            'surveySlug' => $survey->slug,
            'responseId' => $surveyResponse->id,
        ]), array_merge($payload, [
                'nav' => 'save',
            ]),
        )
        ->assertRedirect();

        $this->assertDatabaseHas($survey->response_table, array_merge($payload, [
            'respondent_type' => User::class,
            'respondent_id' => $this->user->id,
            'survey_id' => $survey->id,
            'id' => $surveyResponse->id,
        ]));
    }

    /** @test */
    public function it_can_auto_save_a_survey_response()
    {
        $this->artisan('survey:migration', [
            'document' => './../../../../tests/Fixtures/surveys/' . $this->documentName . '.xml',
        ])->assertExitCode(0);

        $this->artisan('migrate');

        $survey = Survey::first();
        $surveyResponse = $survey->getNewResponse($this->user);
        $surveyResponse->q1 = "Initial Answer";
        $surveyResponse->save();

        $payload = [
            'q1' => 'Survey Response 2 Q1 Answer',
        ];

        $response = $this->put(route('surveys.autosave', [
            'respondentType' => 'sirs-surveys-tests-fixtures-user',
            'respondentId' => $this->user->id,
            'surveySlug' => $survey->slug,
            'responseId' => $surveyResponse->id,
        ]), $payload)
        ->assertOk();

        $this->assertDatabaseHas($survey->response_table, array_merge($payload, [
            'respondent_type' => User::class,
            'respondent_id' => $this->user->id,
            'survey_id' => $survey->id,
            'id' => $surveyResponse->id,
        ]));
    }

    /** @test */
    public function it_can_finalize_a_survey_response()
    {
        Carbon::setTestNow();
        $this->artisan('survey:migration', [
            'document' => './../../../../tests/Fixtures/surveys/' . $this->documentName . '.xml',
        ])->assertExitCode(0);

        $this->artisan('migrate');

        $survey = Survey::first();
        $surveyResponse = $survey->getNewResponse($this->user);
        $surveyResponse->finalized_at = null;
        $surveyResponse->save();

        $payload = [
            'q1' => 'Survey Response 2 Q1 Answer',
        ];

        $response = $this->post(route('survey_post', [
            'respondentType' => 'sirs-surveys-tests-fixtures-user',
            'respondentId' => $this->user->id,
            'surveySlug' => $survey->slug,
            'responseId' => $surveyResponse->id,
        ]), array_merge($payload, [
                'nav' => 'finalize',
            ]),
        )
        ->assertRedirect();

        $this->assertDatabaseHas($survey->response_table, array_merge($payload, [
            'respondent_type' => User::class,
            'respondent_id' => $this->user->id,
            'survey_id' => $survey->id,
            'id' => $surveyResponse->id,
            'finalized_at' => now(),
        ]));
    }

    /** @test */
    public function it_validates_an_invalid_survey_response_on_finalize()
    {
        config()->set('surveys.rulesPath', __DIR__ . '/../Fixtures');

        $this->documentName = "test_survey_with_validation";
        $this->artisan('survey:migration', [
            'document' => './../../../../tests/Fixtures/surveys/' . $this->documentName . '.xml',
        ])->assertExitCode(0);

        $this->artisan('migrate');

        $survey = Survey::whereName("{$this->documentName}1")->first();
        $surveyResponse = $survey->getNewResponse($this->user);
        $surveyResponse->q1 = null;
        $surveyResponse->q2 = null;
        $surveyResponse->finalized_at = null;
        $surveyResponse->save();

        $payload = [
            'q1' => '',
            'q2' => 'asdfasdsa'
        ];

        $response = $this->post(route('survey_post', [
            'respondentType' => 'sirs-surveys-tests-fixtures-user',
            'respondentId' => $this->user->id,
            'surveySlug' => $survey->slug,
            'responseId' => $surveyResponse->id,
        ]), array_merge($payload, [
                'nav' => 'finalize',
            ]),
        )
        ->assertOk();

        $this->assertDatabaseHas($survey->response_table, [
            'respondent_type' => User::class,
            'respondent_id' => $this->user->id,
            'survey_id' => $survey->id,
            'id' => $surveyResponse->id,
            'finalized_at' => null,
            'q1' => null,
            'q2' => null,
        ]);
    }

    /** @test */
    public function it_validates_an_invalid_survey_response_on_autosave()
    {
        Log::shouldReceive('warning')->atLeast();
        Log::partialMock();

        config()->set('surveys.rulesPath', __DIR__ . '/../Fixtures');

        $this->documentName = "test_survey_with_validation";
        $this->artisan('survey:migration', [
            'document' => './../../../../tests/Fixtures/surveys/' . $this->documentName . '.xml',
        ])->assertExitCode(0);

        $this->artisan('migrate');

        $survey = Survey::whereName("{$this->documentName}1")->first();
        $surveyResponse = $survey->getNewResponse($this->user);
        $surveyResponse->q1 = null;
        $surveyResponse->q2 = null;
        $surveyResponse->finalized_at = null;
        $surveyResponse->save();

        $payload = [
            'q1' => '',
            'q2' => 'asdfasdsa'
        ];

        $response = $this->put(route('surveys.autosave', [
            'respondentType' => 'sirs-surveys-tests-fixtures-user',
            'respondentId' => $this->user->id,
            'surveySlug' => $survey->slug,
            'responseId' => $surveyResponse->id,
        ]), $payload)
        ->assertOk();

        $this->assertDatabaseHas($survey->response_table, [
            'respondent_type' => User::class,
            'respondent_id' => $this->user->id,
            'survey_id' => $survey->id,
            'id' => $surveyResponse->id,
            'finalized_at' => null,
            'q1' => null,
            'q2' => null,
        ]);
    }
}
